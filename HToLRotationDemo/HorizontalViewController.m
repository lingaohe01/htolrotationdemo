//
//  HorizontalViewController.m
//  HToLRotationDemo
//
//  Created by lingaohe on 1/10/13.
//  Copyright (c) 2013 ilingaohe. All rights reserved.
//

#import "HorizontalViewController.h"
#import "VerticalViewController.h"

@interface HorizontalViewController ()

@end

@implementation HorizontalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)handleBtnAction:(id)sender
{
  VerticalViewController *verticalViewController = [[VerticalViewController alloc] init];
  [self.navigationController pushViewController:verticalViewController animated:NO];
  [verticalViewController release];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 60)];
  btn.center = self.view.center;
  btn.backgroundColor = [UIColor redColor];
  [btn setTitle:@"弹进一个横屏页面" forState:UIControlStateNormal];
  [btn addTarget:self action:@selector(handleBtnAction:) forControlEvents:UIControlEventTouchUpInside];
  [self.view addSubview:btn];
  [btn release];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
  return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}
- (BOOL)shouldAutorotate
{
  return YES;
}
- (NSUInteger)supportedInterfaceOrientations
{
  return UIInterfaceOrientationMaskPortrait;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
  return UIInterfaceOrientationPortrait;
}
@end
