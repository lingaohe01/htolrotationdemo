//
//  VerticalViewController.m
//  HToLRotationDemo
//
//  Created by lingaohe on 1/10/13.
//  Copyright (c) 2013 ilingaohe. All rights reserved.
//

#import "VerticalViewController.h"
#import "UINavigationController+Autorotate.h"

@interface VerticalViewController ()

@end

@implementation VerticalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  UIViewController *landscapeViewController = [[UIViewController alloc] init];
  [self.navigationController presentModalViewController:landscapeViewController animated:NO];
  [self.navigationController dismissModalViewControllerAnimated:NO];
  [landscapeViewController release];
  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
  if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
      toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
    return YES;
  }else{
    return NO;
  }
}
- (BOOL)shouldAutorotate
{
  return YES;
}
- (NSUInteger)supportedInterfaceOrientations
{
  return UIInterfaceOrientationMaskLandscape;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
  return UIInterfaceOrientationLandscapeLeft;
}
@end
