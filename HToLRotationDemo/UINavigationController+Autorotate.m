//
//  UINavigationController+Autorotate.m
//  RotationDemo
//
//  Created by lingaohe on 1/9/13.
//  Copyright (c) 2013 ilingaohe. All rights reserved.
//

#import "UINavigationController+Autorotate.h"

@implementation UINavigationController (Autorotate)
- (BOOL)shouldAutorotate {
  return [[self.viewControllers lastObject] shouldAutorotate];
}

- (NSUInteger)supportedInterfaceOrientations {
  return [[self.viewControllers lastObject] supportedInterfaceOrientations];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
  UIInterfaceOrientation result = [[self.viewControllers lastObject] preferredInterfaceOrientationForPresentation];
  if (result) {
    return result;
  }else{
    //如果子视图没有设置这个方法，就返回支持除倒转之外的方向
    return UIInterfaceOrientationPortrait|UIInterfaceOrientationLandscapeLeft|UIInterfaceOrientationLandscapeRight;
  }
}
@end
